# ShopStoreApp:
#### It is my first back-end application created for my portfolio. The application is written with Java 17, Spring Boot 2, and Postgres database.
The application is mostly created to make progress with java technology and that was the main reason to create that project. During the creation I've used many different things to learn how to work with them.
I've created a basic REST API and was not really focused on MVC with Thymeleaf and Bootstrap because that was not the main point. Application isn't finished yet, but I'm still working on it all the time and trying to make it better.

### Application allows us to:
Create products, orders, create roles
Register, change password, restore password
Give permissions to users, add roles, change roles, set availability of products (only by admin)
and more.


### How to run:
Open ShopStoreApp main folder -> Open terminal and type: mvn spring-boot:run and then open browser and go to http://localhost:8080
OR
Open the app at: https://shopstore-arek.herokuapp.com

Default login and password
Login: super_admin
Password: password


How to run:
Open ShopStoreApp main folder -> Open terminal and type: mvn spring-boot:run and then open browser and go to http://localhost:8080
OR
Open the app at: https://shopstore-arek.herokuapp.com


### Default login and password
Login: super_admin
Password: password

TODO:
Implement front-end


## Tech/framework used 🔧
<table class="table table-dark table-striped">
                        <thead>
                        <tr>
                          <th scope="col">Tech & framework used</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td>• Java 17</td>
                        </tr>
                        <tr>
                          <td>• Spring Framework (MVC, Data JPA, Security, Validation)</td>
                        </tr>
                        <tr>
                          <td>• Spring Boot</td>
                        </tr>
                         <tr>
                          <td>• Rest API</td>
                        </tr>
                        <tr>
                          <td>• Hibernate/JPA</td>
                        </tr>
                        <tr>
                          <td>• Postgres</td>
                        </tr>
                        <tr>
                          <td>• Docker</td>
                        </tr>
                        <tr>
                          <td>• JUnit tests</td>
                        </tr>
                        <tr>
                          <td>• Thymeleaf</td>
                        </tr>
                        <tr>
                          <td>• Bootstrap</td>
                        </tr>
                        </tbody>
                      </table>
