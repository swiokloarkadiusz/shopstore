package com.bulamen7.shop.service.user;

import com.bulamen7.shop.repository.user.UserEntity;
import com.bulamen7.shop.repository.user.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class UserServiceIT {

    @Autowired
    UserService sut;
    @Autowired
    UserRepository userRepository;

    @BeforeEach
    void teardown() {
        userRepository.deleteAll();
    }

    @Test
    void shouldSaveUser() {
        UserEntity user = new UserEntity("Marek", "login", "pass", "wp@wp.pl",null);
        userRepository.save(user);
        assertThat(sut.findAll().size()).isEqualTo(1);
    }

    @Test
    void shouldFindAll() {
        UserEntity user = new UserEntity("a", "b", "c", "d",null);
        userRepository.save(user);
        assertThat(sut.findAll().size()).isEqualTo(1);
    }
}