package com.bulamen7.shop.service.user;

import com.bulamen7.shop.model.role.Role;
import com.bulamen7.shop.model.user.RegistrationForm;
import com.bulamen7.shop.model.user.UserDto;
import com.bulamen7.shop.repository.user.UserEntity;
import com.bulamen7.shop.repository.user.UserRepository;
import com.bulamen7.shop.repository.user.UserRoleRepository;
import com.bulamen7.shop.service.email.EmailService;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UserServiceTest {
    UserRepository userRepository = mock(UserRepository.class);
    PasswordEncoder passwordEncoder = mock(PasswordEncoder.class);
    UserRoleRepository userRoleRepository = mock(UserRoleRepository.class);
    EmailService emailService = mock(EmailService.class);
    UserService userService = new UserService(userRepository, passwordEncoder, userRoleRepository, emailService);

    @Test
    void shouldSaveUser() {
        RegistrationForm registrationForm = new RegistrationForm("Marek", "login", "Password1", "wp@wp.pl", "Password1");
        userService.saveUser(registrationForm, Role.USER);
        verify(userRepository).save(any());
    }

    @Test
    void shouldFindById() {
        UserEntity user = new UserEntity("Marek", "login", "pass", "wp@wp.pl", null);
        //when
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        UserDto expectedUser = userService.findById(1L);
        //then
        assertThat(expectedUser).isEqualTo(new UserDto(null, "Marek", "wp@wp.pl"));
    }

    @Test
    void shouldFindAll() {
        UserEntity user = new UserEntity("Marek", "login", "pass", "wp@wp.pl", null);
        UserEntity user2 = new UserEntity("Marek5", "login5", "pass5", "w5p@wp.pl", null);
        //when
        when(userRepository.findAll()).thenReturn(List.of(user, user2));
        List<UserDto> expectedUsers = userService.findAll();
        //then
        assertThat(expectedUsers.size()).isEqualTo(2);
    }

    @Test
    void shouldDeleteUserById() {
        userService.deleteUserById(5L);
        verify(userRepository).deleteById(any());
    }


}
