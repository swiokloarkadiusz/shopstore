package com.bulamen7.shop.model.order;

import com.bulamen7.shop.model.product.ProductDto;

import java.util.List;

public record OrderDto(Long id,
                       Long userId,
                       List<ProductDto> products) {

}