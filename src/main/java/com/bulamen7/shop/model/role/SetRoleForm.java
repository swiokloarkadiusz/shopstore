package com.bulamen7.shop.model.role;

public class SetRoleForm {
    private Role role;
    //TODO CHANGE to id
    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
