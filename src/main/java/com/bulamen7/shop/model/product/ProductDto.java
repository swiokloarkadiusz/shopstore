package com.bulamen7.shop.model.product;


import java.math.BigDecimal;

public final class ProductDto {
    private final Long id;
    private final String name;
    private final BigDecimal price;
    private final String description;
    private boolean available;

    public ProductDto(Long id, String name, BigDecimal price, String description, Boolean available) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.available = available;
    }

    public Long id() {
        return id;
    }

    public String name() {
        return name;
    }

    public BigDecimal price() {
        return price;
    }

    public String description() {
        return description;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "ProductDto[" +
                "id=" + id + ", " +
                "name=" + name + ", " +
                "price=" + price + ", " +
                "description=" + description + ']';
    }

}
