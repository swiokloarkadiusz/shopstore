package com.bulamen7.shop.model.user;

public record UserDto(Long id,
                      String name,
                      String mail) {
}
