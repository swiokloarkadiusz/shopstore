package com.bulamen7.shop.service.user.exception;

public class EmailAlreadyExistsException extends RuntimeException{
    private static final String EXCEPTION_MESSAGE = "Email %s is taken. ";

    public EmailAlreadyExistsException(String message) {
        super(String.format(EXCEPTION_MESSAGE, message));
    }
}
