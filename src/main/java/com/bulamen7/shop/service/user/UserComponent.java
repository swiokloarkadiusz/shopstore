package com.bulamen7.shop.service.user;

import com.bulamen7.shop.model.role.Role;
import com.bulamen7.shop.model.user.RegistrationForm;
import com.bulamen7.shop.repository.user.UserRole;
import com.bulamen7.shop.repository.user.UserRoleRepository;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
class UserComponent {
    private final UserRoleRepository userRoleRepository;
    private final UserService userService;

    UserComponent(UserRoleRepository userRoleRepository, UserService userService) {
        this.userRoleRepository = userRoleRepository;
        this.userService = userService;
    }

    @PostConstruct
    void init() {
        initRoles();
        initSuperAdmin();
    }

    private void initRoles() {
        for (Role role : Role.values()) {
            if (!userRoleRepository.existsByRole(role)) {
                userRoleRepository.save(new UserRole(role));
            }
        }
    }

    void initSuperAdmin() {
        if (!userService.existsByLogin("super_admin")) {
            userService.saveUser(new RegistrationForm("Admin", "super_admin", "password", "zxc@gmail.com", "password"), Role.ADMIN);
        }
    }
}

//admin nadaje role uzyttkownikom

//shopstoreapp1 Qwertzzz12345