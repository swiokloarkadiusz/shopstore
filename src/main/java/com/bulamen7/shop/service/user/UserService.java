package com.bulamen7.shop.service.user;

import com.bulamen7.shop.model.authentication.NewPasswordForm;
import com.bulamen7.shop.model.role.Role;
import com.bulamen7.shop.model.user.RegistrationForm;
import com.bulamen7.shop.model.user.UserDto;
import com.bulamen7.shop.repository.user.UserEntity;
import com.bulamen7.shop.repository.user.UserRepository;
import com.bulamen7.shop.repository.user.UserRole;
import com.bulamen7.shop.repository.user.UserRoleRepository;
import com.bulamen7.shop.service.ModelMapper;
import com.bulamen7.shop.service.email.EmailService;
import com.bulamen7.shop.service.user.exception.EmailAlreadyExistsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserRoleRepository userRoleRepository;
    private final EmailService emailService;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, UserRoleRepository userRoleRepository, EmailService emailService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRoleRepository = userRoleRepository;
        this.emailService = emailService;
    }

    public void saveUser(RegistrationForm newUserForm, Role role) {
        UserRole userRole = userRoleRepository
                .findByRole(role)
                .orElseThrow(() -> new IllegalStateException());

        UserEntity userEntity = new UserEntity(
                newUserForm.getName(),
                newUserForm.getLogin(),
                passwordEncoder.encode(newUserForm.getPassword()),
                newUserForm.getEmail(),
                userRole);

        if (userRepository.existsByLogin(newUserForm.getLogin())) {
            throw new IllegalStateException("Duplicated User");
        }
        if (userRepository.existsByMail(newUserForm.getEmail())) {
            throw new EmailAlreadyExistsException(newUserForm.getEmail());
        }
        userRepository.save(userEntity);

        try {
            emailService.sendEmail(userEntity.getMail(), "email/newUserEmailTemplate");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void changePassword(NewPasswordForm newPasswordForm, String login) {
        Optional<UserEntity> byLogin = userRepository.findByLogin(login);
        byLogin.ifPresent(userEntity -> userEntity.setPassword(passwordEncoder.encode(newPasswordForm.getPassword())));
    }

    public UserDto findById(Long id) {
        return userRepository.findById(id)
                .map(ModelMapper::map)
                .orElseThrow(() -> new IllegalStateException("User not found"));
    }

    public List<UserDto> findAll() {
        return userRepository.findAll().stream()
                .map(ModelMapper::map).toList();
    }

    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }

    public boolean existsByLogin(String login) {
        return userRepository.existsByLogin(login);
    }

    public boolean existsByEmail(String email) {
        return userRepository.existsByMail(email);
    }

    public void resetPassword(String email) {
        UserEntity userEntity = userRepository.findByMail(email)
                .orElseThrow(() -> new RuntimeException("there is no user with email: " + email));
        UUID token = UUID.randomUUID();
        userEntity.setResetToken(token.toString());
        try {
            Context context = new Context();
            context.setVariable("token", token);
            emailService.sendEmail(email, "email/resetPasswordEmailTemplate.html", context);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void updatePassword(String token, String newPassword) {
        UserEntity userEntity = userRepository
                .findByResetToken(token)
                .orElseThrow(() -> new RuntimeException("token is incorrect"));
        userEntity.setPassword(passwordEncoder.encode(newPassword));
        userEntity.setResetToken(null);
    }

    public void addRole(String login, Role role) {
        UserEntity userEntity = userRepository
                .findByLogin(login)
                .orElseThrow(() -> new RuntimeException("User with given login doesnt exists"));
        UserRole userRole = userRoleRepository
                .findByRole(role)
                .orElseThrow(() -> new RuntimeException("Couldnt find role " + role));
        userEntity.addRole(userRole);
    }
}













