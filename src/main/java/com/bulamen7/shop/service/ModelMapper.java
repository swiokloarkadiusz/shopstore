package com.bulamen7.shop.service;

import com.bulamen7.shop.model.order.OrderDto;
import com.bulamen7.shop.model.product.ProductDto;
import com.bulamen7.shop.model.user.UserDto;
import com.bulamen7.shop.repository.order.OrderEntity;
import com.bulamen7.shop.repository.product.ProductEntity;
import com.bulamen7.shop.repository.user.UserEntity;

import java.util.List;

public class ModelMapper {
    public static OrderDto map(OrderEntity entity) {
        List<ProductEntity> products = entity.getProducts();

        List<ProductDto> productsDtos = products.stream()
                .map(ModelMapper::map).toList();

        return new OrderDto(
                entity.getId(),
                entity.getUser().getId(),
                productsDtos);
    }

    public static ProductDto map(ProductEntity product) {
        return new ProductDto(
                product.getId(),
                product.getName(),
                product.getPrice(),
                product.getDescription(),
                product.isAvailable());
    }

    public static UserDto map(UserEntity user) {
        return new UserDto(user.getId(),
                user.getName(),
                user.getMail());
    }
}
