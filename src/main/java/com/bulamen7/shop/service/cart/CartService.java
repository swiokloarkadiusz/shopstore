package com.bulamen7.shop.service.cart;

import com.bulamen7.shop.model.product.ProductDto;
import com.bulamen7.shop.repository.cart.CartEntity;
import com.bulamen7.shop.repository.cart.CartRepository;
import com.bulamen7.shop.repository.product.ProductEntity;
import com.bulamen7.shop.repository.product.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CartService {

    private final CartRepository cartRepository;
    private final ProductRepository productRepository;

    public CartService(CartRepository cartRepository, ProductRepository productRepository) {
        this.cartRepository = cartRepository;
        this.productRepository = productRepository;
    }
    public void addProduct(Long productId, String username){
        CartEntity cartEntity = cartRepository.findByUserLogin(username).orElseThrow(() -> new IllegalStateException());
        ProductEntity product = productRepository.findById(productId).orElseThrow(()-> new IllegalStateException());
        cartEntity.getProducts().add(product);
    }

    public List<ProductDto> findByUsername(String username) {

        CartEntity cart = cartRepository
                .findByUserLogin(username)
                .orElseGet(() -> createNewCart(username));

        return cart.getProducts()
                .stream()
                .map(product -> new ProductDto(product.getId(), product.getName(), product.getPrice(), product.getDescription(), product.isAvailable()))
                .collect(Collectors.toList());
    }

    private CartEntity createNewCart(String username) {
        CartEntity cart = new CartEntity(username);

        return cartRepository.save(cart);
    }
}
