package com.bulamen7.shop.controller.product;

import com.bulamen7.shop.model.product.NewProductForm;
import com.bulamen7.shop.model.product.ProductDto;
import com.bulamen7.shop.model.product.SearchByKeywordForm;
import com.bulamen7.shop.model.product.UpdateProductForm;
import com.bulamen7.shop.service.product.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/products")
class ProductController {
    private final ProductService productService;
    private final NewProductFormValidator validator;

    ProductController(ProductService productService, NewProductFormValidator validator) {
        this.productService = productService;
        this.validator = validator;
    }

    @InitBinder(value = "newProduct")
    public void init(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @GetMapping
    ModelAndView homePage() {
        ModelAndView modelAndView = new ModelAndView("product/index");
        List<ProductDto> products = productService.findOnlyAvailableProducts();
        modelAndView.addObject("productsList", products);
        modelAndView.addObject("searchedByKeyword", new SearchByKeywordForm());
        return modelAndView;
    }

    @GetMapping("/{id}")
    ModelAndView findProduct(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("product/editProduct");
        ProductDto product = productService.findById(id);
        modelAndView.addObject("product", product);
        modelAndView.addObject("updateForm", new UpdateProductForm());
        return modelAndView;
    }

    @GetMapping("/add")
    ModelAndView addProductPage() {
        ModelAndView modelAndView = new ModelAndView("product/addProduct");
        modelAndView.addObject("newProduct", new NewProductForm());
        return modelAndView;
    }

    @PostMapping("/add")
    ModelAndView addProduct(@ModelAttribute("newProduct") @Validated NewProductForm newProduct, BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            productService.addProduct(newProduct);
            return new ModelAndView("redirect:/products");
        }
        return new ModelAndView("product/addProduct");
    }


    @PostMapping("/{id}/patch")
    String patchProduct(@PathVariable Long id, UpdateProductForm updateProductForm) {
        productService.patchUpdate(id, updateProductForm);
        return "redirect:/products";
    }

    @PostMapping("/{id}/put")
    String putProduct(@PathVariable Long id, UpdateProductForm updateProductForm) {
        productService.putUpdate(id, updateProductForm);
        return "redirect:/products";
    }

    @PostMapping
    ModelAndView filterProductsByName(@ModelAttribute("searchedByKeyword") SearchByKeywordForm form) {
        ModelAndView modelAndView = new ModelAndView("product/index");
        List<ProductDto> products = productService.findOnlyAvailableProductsByKeyword(form.getKeyword());
        modelAndView.addObject("searchedByKeyword", new SearchByKeywordForm());
        modelAndView.addObject("productsList", products);
        return modelAndView;
    }

    @GetMapping("/search")
    ModelAndView filterProductByNameGet(@RequestParam(name = "keyword") String keyword) {
        ModelAndView modelAndView = new ModelAndView("product/index");
        List<ProductDto> products = productService.findOnlyAvailableProductsByKeyword(keyword);
        modelAndView.addObject("searchedByKeyword", new SearchByKeywordForm());
        modelAndView.addObject("productsList", products);
        return modelAndView;
    }
}
