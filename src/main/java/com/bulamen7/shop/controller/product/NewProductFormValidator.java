package com.bulamen7.shop.controller.product;

import com.bulamen7.shop.model.product.NewProductForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.math.BigDecimal;

@Component
class NewProductFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return NewProductForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        NewProductForm form = (NewProductForm) target;
        if (form.getName() == null || form.getName().equals("")) {
            errors.rejectValue("name", "validator.field.blank");
        }
        if (form.getDescription() == null || form.getDescription().equals("")) {
            errors.rejectValue("description", "validator.field.blank");
        }
        if (!isNumber(form.getPrice())) {
            errors.rejectValue("price", "invalid.number");
        } else {
            BigDecimal price = new BigDecimal(form.getPrice());
            if (price == null || price.compareTo(BigDecimal.ZERO) <= 0) {
                errors.rejectValue("price", "validator.field.blank");
                errors.rejectValue("price", "validator.newProductFormValidator.field.priceLowerThanZero");
            }
        }
    }

    private boolean isNumber(String number) {
        try {
            Double.parseDouble(number);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
