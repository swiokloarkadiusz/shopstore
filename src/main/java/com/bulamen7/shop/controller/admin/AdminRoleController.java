package com.bulamen7.shop.controller.admin;

import com.bulamen7.shop.model.role.Role;
import com.bulamen7.shop.model.role.SetRoleForm;
import com.bulamen7.shop.service.user.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin/role")
public class AdminRoleController {
    private final UserService userService;

    public AdminRoleController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    ModelAndView setRolePage() {
        ModelAndView modelAndView = new ModelAndView("admin/setRoles");
        modelAndView.addObject("addRoleForm", new SetRoleForm());
        modelAndView.addObject("roles", Role.values());
        return modelAndView;
    }

    @PostMapping
    String addRole(@ModelAttribute SetRoleForm setRoleForm) {
        userService.addRole(setRoleForm.getLogin(), setRoleForm.getRole());
        return "redirect:/";
    }
}
