package com.bulamen7.shop.controller.admin;

import com.bulamen7.shop.model.product.ProductDto;
import com.bulamen7.shop.model.product.SearchByKeywordForm;
import com.bulamen7.shop.service.product.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
@Controller
@RequestMapping("/admin/products")
public class AdminProductController {
    private final ProductService productService;

    public AdminProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    ModelAndView homePage() {
        ModelAndView modelAndView = new ModelAndView("admin/products");
        List<ProductDto> products = productService.findAll();
        modelAndView.addObject("productsList", products);
        modelAndView.addObject("searchedByKeyword", new SearchByKeywordForm());
        return modelAndView;
    }

    @GetMapping("/{id}/markAsAvailable")
    String setAsAvailable(@PathVariable Long id){
        productService.setAvailableToTrue(id);
        return "redirect:/admin/products";
    }

    @GetMapping("/{id}/markAsNotAvailable")
    String setAsNotAvailable(@PathVariable Long id){
        productService.setAvailableToFalse(id);
        return "redirect:/admin/products";
    }

    @GetMapping("/{id}/delete")
    String deleteProduct(@PathVariable Long id) {
        productService.deleteById(id);
        return "redirect:/admin/products";
    }
}
