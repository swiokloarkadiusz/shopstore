package com.bulamen7.shop.controller.admin;

import com.bulamen7.shop.service.order.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin/orders")
public class AdminOrderController {

    private final OrderService orderService;

    public AdminOrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping
    ModelAndView getAllOrders() {
        ModelAndView modelAndView = new ModelAndView("admin/orders");
        modelAndView.addObject("orders", orderService.findAll());
        return modelAndView;
    }
}
