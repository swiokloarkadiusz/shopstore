package com.bulamen7.shop.controller.user;

import com.bulamen7.shop.service.user.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/resetPassword")
public class ResetPasswordController {
    private final UserService userService;

    public ResetPasswordController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    ModelAndView resetPasswordPage() {
        ModelAndView modelAndView = new ModelAndView("resetPassword/index");
        modelAndView.addObject(new ResetPasswordForm());
        return modelAndView;
    }

    //refactor me: logic shouldnt be in controller
    @PostMapping
    String resetPassword(@ModelAttribute ResetPasswordForm resetPasswordForm) {
        if (userService.existsByEmail(resetPasswordForm.getEmail())) {
            userService.resetPassword(resetPasswordForm.getEmail());
        };
        return "redirect:/login";
    }

    @GetMapping("/{token}")
    ModelAndView setNewPasswordPage(@PathVariable String token) {
        ModelAndView modelAndView = new ModelAndView("resetPassword/newPassword");
        modelAndView.addObject("token", token);
        modelAndView.addObject("setNewPassword", new SetNewPasswordForm());
        return modelAndView;
    }

    @PostMapping("/{token}")
    String setNewPassword(@ModelAttribute SetNewPasswordForm setNewPasswordForm, @PathVariable String token) {
        userService.updatePassword(token, setNewPasswordForm.getPassword());
        return "redirect:/index";
    }

}



