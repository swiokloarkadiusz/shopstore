package com.bulamen7.shop.controller.user;

public class SetNewPasswordForm {
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
