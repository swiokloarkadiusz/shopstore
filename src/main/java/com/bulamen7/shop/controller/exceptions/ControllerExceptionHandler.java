package com.bulamen7.shop.controller.exceptions;

import com.bulamen7.shop.service.order.exception.OrderNotFoundException;
import com.bulamen7.shop.service.product.exception.ProductNotFoundException;
import com.bulamen7.shop.service.user.exception.EmailAlreadyExistsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.ModelAndView;

import java.net.BindException;

//@ControllerAdvice(annotations = Controller.class)
public class ControllerExceptionHandler {

    @ExceptionHandler(OrderNotFoundException.class)
    ModelAndView orderException(OrderNotFoundException exception) {
        return getModelAndView(exception.getMessage());
    }

    @ExceptionHandler(ProductNotFoundException.class)
    ModelAndView productException(ProductNotFoundException exception) {
        return getModelAndView(exception.getMessage());
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    ModelAndView methodArgumentException(MethodArgumentTypeMismatchException exception) {
        return getModelAndView(exception.getMessage());
    }

    @ExceptionHandler(IllegalStateException.class)
    ModelAndView illegalArgumentException(IllegalStateException exception) {
        return getModelAndView(exception.getMessage());
    }

    @ExceptionHandler(NumberFormatException.class)
    ModelAndView numberFormatException(NumberFormatException exception) {
        return getModelAndView(exception.getMessage());
    }

    @ExceptionHandler(EmailAlreadyExistsException.class)
    ModelAndView emailAlreadyExistsException(EmailAlreadyExistsException exception) {
        return getModelAndView(exception.getMessage());
    }

    @ExceptionHandler(RuntimeException.class)
    ModelAndView runtimeException(RuntimeException exception) {
        return getModelAndView(exception.getMessage());
    }
    @ExceptionHandler(BindException.class)
    ModelAndView illegal(BindException exception) {
        return getModelAndView(exception.getMessage());
    }

    private ModelAndView getModelAndView(String exception) {
        ModelAndView modelAndView = new ModelAndView("error/errorPage");
        modelAndView.addObject("message", exception);
        return modelAndView;
    }
}
