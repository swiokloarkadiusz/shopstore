package com.bulamen7.shop.component;

import com.bulamen7.shop.model.product.ProductDto;
import com.bulamen7.shop.service.cart.CartService;
import com.bulamen7.shop.service.order.OrderService;
import com.bulamen7.shop.service.product.ProductService;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
@Scope(scopeName = WebApplicationContext.SCOPE_SESSION,
        proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Cart {
    private String username;
    private List<ProductDto> products;
    private final OrderService orderService;
    private final ProductService productService;
    private final CartService cartService;


    public Cart(OrderService orderService, ProductService productService, CartService cartService) {
        this.cartService = cartService;
        System.out.println("wake up CART");
        this.products = new ArrayList<>();
        this.orderService = orderService;
        this.productService = productService;
    }

    @PostConstruct
    public void init() {
        username = getUserLogin().getUsername();
        products = cartService.findByUsername(username);
    }

    public void addProduct(Long id) {
        ProductDto byId = productService.findById(id);
        products.add(byId);
        cartService.addProduct(id, username);
    }

    public void removeProduct(Long id) {
        products.removeIf(product -> Objects.equals(product.id(), id));
    }

    public List<ProductDto> findAllProducts() {
        return products;
    }

    public void submitCart() {
        List<Long> productIds = products.stream()
                .map(ProductDto::id).toList();
        UserDetails userDetails = getUserLogin();
        if (productIds.size() == 0) {
            throw new IllegalStateException("Cart cant be empty");
        } else {
            orderService.createOrder(productIds, userDetails.getUsername());
        }
        products.clear();
    }

    public BigDecimal totalPrice() {
        BigDecimal sum = BigDecimal.ZERO;
        for (ProductDto amt : products) {
            sum = sum.add(amt.price());
        }
        return sum;
    }

    private UserDetails getUserLogin() {
        return (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}


