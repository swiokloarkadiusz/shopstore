package com.bulamen7.shop.repository.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
    @Query(value = "select p from product_entity p where concat(p.name, p.description) LIKE %:keyword%")
    List<ProductEntity> findByKeyword(String keyword);
    List<ProductEntity> findByIsAvailableTrue();

}
