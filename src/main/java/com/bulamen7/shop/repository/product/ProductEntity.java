package com.bulamen7.shop.repository.product;

import com.bulamen7.shop.repository.order.OrderEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "product_entity")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private BigDecimal price;
    private String description;
    private LocalDateTime createDateTime = LocalDateTime.now();
    private boolean isAvailable;

    @ManyToMany
    @JoinTable(name = "order_product", joinColumns = @JoinColumn(name = "product_id"), inverseJoinColumns = @JoinColumn(name = "order_id"))
    private List<OrderEntity> orderEntities = new ArrayList<>();

    public ProductEntity() {
    }

    public ProductEntity(String name, BigDecimal price, String description, boolean isAvailable) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.isAvailable = isAvailable;
    }

    public void markAsNotAvailable() {
        isAvailable = false;
    }

    public void markAsAvailable() {
        isAvailable = true;
    }

    public void addOrder(OrderEntity orderEntity) {
        this.orderEntities.add(orderEntity);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public boolean isAvailable() {
        return isAvailable;
    }
}
