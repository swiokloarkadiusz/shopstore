package com.bulamen7.shop.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    boolean existsByLogin(String login);

    boolean existsByMail(String mail);

    boolean existsByResetToken(String token);

    Optional<UserEntity> findByResetToken(String token);

    Optional<UserEntity> findByMail(String email);

    Optional<UserEntity> findByLogin(String login);
}
