package com.bulamen7.shop.repository.user;

import com.bulamen7.shop.model.role.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    boolean existsByRole(Role role);

    Optional<UserRole> findByRole(Role role);


}
