package com.bulamen7.shop.repository.user;

import com.bulamen7.shop.repository.order.OrderEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity(name = "users")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String login;
    private String password;
    @Column(unique = true)
    private String mail;
    private String resetToken;

    @ManyToMany
    @JoinColumn(name = "id_role")
    public Set<UserRole> userRoles = new HashSet<>();

    @OneToMany(mappedBy = "user")
    private List<OrderEntity> orderEntities;

    public UserEntity() {
    }

    public UserEntity(String name, String login, String password, String mail, UserRole userRole) {
        this.name = name;
        this.login = login;
        this.password = password;
        this.mail = mail;
        this.userRoles.add(userRole);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }
    public Set<UserRole> getUserRoles() {
        return userRoles;
    }


    public void addRole(UserRole userRole){
        userRoles.add(userRole);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", mail='" + mail + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity user = (UserEntity) o;
        return Objects.equals(id, user.id) && Objects.equals(name, user.name) && Objects.equals(login, user.login) && Objects.equals(password, user.password) && Objects.equals(mail, user.mail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, login, password, mail);
    }


}
