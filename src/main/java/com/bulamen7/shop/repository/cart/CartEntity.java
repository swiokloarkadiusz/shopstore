package com.bulamen7.shop.repository.cart;

import com.bulamen7.shop.repository.product.ProductEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class CartEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String userLogin;
    @ManyToMany
    @JoinTable(name = "cart_product", joinColumns = @JoinColumn(name = "product_id"), inverseJoinColumns = @JoinColumn(name = "cart_id"))
    private List<ProductEntity> products = new ArrayList<>();

    public CartEntity() {
    }

    public CartEntity(String userLogin) {
        this.userLogin = userLogin;
    }

    public List<ProductEntity> getProducts() {
        return products;
    }
}
